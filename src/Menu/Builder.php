<?php

declare(strict_types=1);

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Security\Core\Security;

final class Builder
{
    private FactoryInterface $factory;
    private Security $security;

    public function __construct(FactoryInterface $factory, Security $security)
    {
        $this->factory  = $factory;
        $this->security = $security;
    }

    public function headerMenu(array $options): ItemInterface
    {
        $isAdmin   = $this->security->isGranted('ROLE_ADMIN');
        $isCounter = $this->security->isGranted('ROLE_COUNTER');
        $isDoctor  = $this->security->isGranted('ROLE_DOCTOR');

        $menu = $this->factory->createItem('root');

        $menu->setChildrenAttribute('class', 'nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0 text-secondary');

        $menu->addChild('Home', ['route' => 'home'])
            ->setLinkAttribute('class', 'nav-link px-2 text-secondary');

        if ($isAdmin || $isCounter) {
            $menu->addChild('Patients', [
                'route'  => 'patient_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'patient_get_single',
                        ],
                        [
                            'route' => 'patient_create',
                        ],
                        [
                            'route' => 'patient_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');
        }

        if ($isAdmin) {
            $menu->addChild('Doctors', [
                'route'  => 'doctor_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'doctor_get_single',
                        ],
                        [
                            'route' => 'doctor_create',
                        ],
                        [
                            'route' => 'doctor_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');
        }

        if ($isAdmin || $isCounter || $isDoctor) {
            $menu->addChild('Examinations', [
                'route'  => 'examination_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'examination_get_single',
                        ],
                        [
                            'route' => 'examination_create',
                        ],
                        [
                            'route' => 'examination_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');
        }

        if ($isAdmin) {
            $menu->addChild('Doctor Types', [
                'route'  => 'doctor_type_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'doctor_type_get_single',
                        ],
                        [
                            'route' => 'doctor_type_create',
                        ],
                        [
                            'route' => 'doctor_type_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');

            $menu->addChild('Countries', [
                'route'  => 'country_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'country_get_single',
                        ],
                        [
                            'route' => 'country_create',
                        ],
                        [
                            'route' => 'country_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');

            $menu->addChild('Users', [
                'route'  => 'user_list',
                'extras' => [
                    'routes' => [
                        [
                            'route' => 'user_get_single',
                        ],
                        [
                            'route' => 'user_create',
                        ],
                        [
                            'route' => 'user_update',
                        ],
                    ],
                ],
            ])->setLinkAttribute('class', 'nav-link px-2 text-secondary');
        }

        return $menu;
    }
}