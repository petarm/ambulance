<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Entity\Patient;
use App\Repository\CountryRepository;
use App\Repository\PatientRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PatientFixtures extends Fixture
{
    private PatientRepository $patientRepository;

    private CountryRepository $countryRepository;

    public function __construct(PatientRepository $patientRepository, CountryRepository $countryRepository)
    {
        $this->patientRepository = $patientRepository;
        $this->countryRepository = $countryRepository;
    }

    public function load(ObjectManager $manager)
    {
        $country = new Country();

        $country->setName('Serbia');

        $country = $this->countryRepository->save($country);

        $patient = new Patient();

        $patient->setFirstName('Patient');
        $patient->setLastName('Patient');
        $patient->setLocation($country);
        $patient->setJmbg('2509996710243');
        $patient->setNote('Sample note');

        $this->patientRepository->save($patient);
    }
}
