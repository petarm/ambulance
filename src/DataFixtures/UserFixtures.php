<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    private UserRepository $userRepository;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager)
    {
        $this->createAdminUser();
        $this->createCounterUser();
    }

    private function createAdminUser(): void
    {
        $user = new User();

        $user->setFirstName('Admin');
        $user->setLastName('Admin');
        $user->setUsername('admin');
        $user->setRoles([
            'ROLE_ADMIN',
        ]);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'admin'
        ));

        $this->userRepository->save($user);
    }

    private function createCounterUser(): void
    {
        $user = new User();

        $user->setFirstName('Counter');
        $user->setLastName('Counter');
        $user->setUsername('counter');
        $user->setRoles([
            'ROLE_COUNTER',
        ]);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'counter'
        ));

        $this->userRepository->save($user);
    }
}
