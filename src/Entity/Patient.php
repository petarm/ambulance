<?php

namespace App\Entity;

use App\Repository\PatientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[A-Z]/",
     *     message="First name must begin with a capital letter."
     * )
     * @Assert\Length(
     *      max=100,
     * )
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[A-Z]/",
     *     message="Last name must begin with a capital letter."
     * )
     * @Assert\Length(
     *      max=100,
     * )
     */
    private string $lastName;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Country $location;

    /**
     * @ORM\Column(type="string", length=13)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^\d{13}$/",
     *     message="JMBG must be a valid JMBG."
     * )
     */
    private string $jmbg;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      max=1000,
     * )
     */
    private string $note;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLocation(): Country
    {
        return $this->location;
    }

    public function setLocation(Country $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getJmbg(): ?string
    {
        return $this->jmbg;
    }

    public function setJmbg(string $jmbg): self
    {
        $this->jmbg = $jmbg;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }
}
