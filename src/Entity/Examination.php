<?php

namespace App\Entity;

use App\Repository\ExaminationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ExaminationRepository::class)
 */
class Examination
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Patient $patient;

    /**
     * @ORM\ManyToOne(targetEntity=Doctor::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Doctor $doctor;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $appointment;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      max=2000,
     * )
     */
    private ?string $diagnosis;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $performed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatient(): Patient
    {
        return $this->patient;
    }

    public function setPatient(Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getDoctor(): ?Doctor
    {
        return $this->doctor;
    }

    public function setDoctor(Doctor $doctor): self
    {
        $this->doctor = $doctor;

        return $this;
    }

    public function getAppointment(): DateTimeInterface
    {
        return $this->appointment;
    }

    public function setAppointment(DateTimeInterface $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getDiagnosis(): ?string
    {
        return $this->diagnosis;
    }

    public function setDiagnosis(?string $diagnosis): self
    {
        $this->diagnosis = $diagnosis;

        return $this;
    }

    public function getPerformed(): bool
    {
        return $this->performed;
    }

    public function setPerformed(bool $performed): self
    {
        $this->performed = $performed;

        return $this;
    }
}
