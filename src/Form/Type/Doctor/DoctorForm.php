<?php

declare(strict_types=1);

namespace App\Form\Type\Doctor;

use App\Entity\Doctor;
use App\Entity\DoctorType;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class DoctorForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', EntityType::class, [
                'class'        => DoctorType::class,
                'choice_label' => 'name',
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('user', EntityType::class, [
                'class'        => User::class,
                'choice_label' => function (User $user) {
                    return $user->getFirstName() . ' ' . $user->getLastName();
                },
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success mt-3'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Doctor::class,
            'empty_data' => new Doctor(),
        ]);
    }
}