<?php

declare(strict_types=1);

namespace App\Form\Type\Registration;

use App\Entity\DoctorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class RegisterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('first_name', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('last_name', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('username', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('plainPassword', PasswordType::class, [
                'required'   => true,
                'empty_data' => '',
                'label'      => 'Password',
                'attr'       => ['class' => 'mt-2'],
            ])
            ->add('doctor_type', EntityType::class, [
                'class'        => DoctorType::class,
                'choice_label' => 'name',
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('register', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success mt-3'],
            ]);
    }
}