<?php

declare(strict_types=1);

namespace App\Form\Type\Examination;

use Symfony\Component\Form\FormBuilderInterface;

final class DoctorExaminationForm extends ExaminationForm
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->get('patient')
            ->setDisabled(true);

        $builder
            ->get('doctor')
            ->setDisabled(true);

        $builder
            ->get('appointment')
            ->setDisabled(true);
    }
}