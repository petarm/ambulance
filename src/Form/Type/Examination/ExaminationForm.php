<?php

declare(strict_types=1);

namespace App\Form\Type\Examination;

use App\Entity\Doctor;
use App\Entity\Examination;
use App\Entity\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExaminationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('patient', EntityType::class, [
                'class'        => Patient::class,
                'choice_label' => function (Patient $patient) {
                    return $patient->getFirstName() . ' ' . $patient->getLastName();
                },
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('doctor', EntityType::class, [
                'class'        => Doctor::class,
                'choice_label' => function (Doctor $doctor) {
                    return $doctor->getUser()->getFirstName() . ' ' . $doctor->getUser()->getLastName();
                },
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('diagnosis', TextareaType::class, [
                'required' => false,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('appointment', DateTimeType::class, [
                'required' => true,
                'widget'   => 'single_text',
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('performed', CheckboxType::class, [
                'required'   => false,
                'attr'       => ['class' => 'mt-2'],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success mt-3'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Examination::class,
            'empty_data' => new Examination(),
        ]);
    }
}