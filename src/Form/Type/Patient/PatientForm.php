<?php

declare(strict_types=1);

namespace App\Form\Type\Patient;

use App\Entity\Country;
use App\Entity\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class PatientForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('first_name', TextType::class, [
                'required' => true,
                'attr'     => ['class'   => 'mt-2'],
            ])
            ->add('last_name', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('location', EntityType::class, [
                'class'        => Country::class,
                'choice_label' => 'name',
                'required'     => true,
                'attr'         => ['class' => 'mt-2'],
            ])
            ->add('jmbg', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('note', TextareaType::class, [
                'required' => false,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success mt-3'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Patient::class,
            'empty_data' => new Patient(),
        ]);
    }
}