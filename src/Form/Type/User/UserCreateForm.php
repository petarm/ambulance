<?php

declare(strict_types=1);

namespace App\Form\Type\User;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCreateForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('first_name', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('last_name', TextType::class, [
                'required' => true,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('roles', ChoiceType::class, [
                'choices'  => [
                    'Admin'   => 'ROLE_ADMIN',
                    'Counter' => 'ROLE_COUNTER',
                ],
                'multiple' => true,
                'required' => false,
                'attr'     => ['class' => 'mt-2'],
            ])
            ->add('plainPassword', PasswordType::class, [
                'required'   => true,
                'empty_data' => '',
                'label'      => 'Password',
                'attr'       => ['class' => 'mt-2'],
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success mt-3'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'empty_data' => new User(),
        ]);
    }
}