<?php

declare(strict_types=1);

namespace App\Form\Type\User;

use Symfony\Component\Form\FormBuilderInterface;

final class UserUpdateForm extends UserCreateForm
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->get('plainPassword')
            ->setRequired(false);
    }
}