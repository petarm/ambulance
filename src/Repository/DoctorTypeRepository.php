<?php

namespace App\Repository;

use App\Entity\DoctorType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DoctorType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoctorType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoctorType[]    findAll()
 * @method DoctorType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctorTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoctorType::class);
    }

    public function save(DoctorType $doctorType): DoctorType
    {
        $this->_em->persist($doctorType);
        $this->_em->flush();

        return $doctorType;
    }

    public function delete(DoctorType $doctorType): void
    {
        $this->_em->remove($doctorType);
        $this->_em->flush();
    }
}
