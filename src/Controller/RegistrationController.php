<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Doctor;
use App\Entity\User;
use App\Form\Type\Registration\RegisterForm;
use App\Repository\DoctorRepository;
use App\Repository\DoctorTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

final class RegistrationController extends AbstractController
{
    private UserRepository $userRepository;

    private UserPasswordEncoderInterface $passwordEncoder;

    private DoctorTypeRepository $doctorTypeRepository;

    private DoctorRepository $doctorRepository;

    private Security $security;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        DoctorTypeRepository $doctorTypeRepository,
        DoctorRepository $doctorRepository,
        Security $security
    )
    {
        $this->userRepository  = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->doctorTypeRepository = $doctorTypeRepository;
        $this->doctorRepository = $doctorRepository;
        $this->security = $security;
    }

    public function register(Request $request): Response
    {
        $form = $this->createForm(RegisterForm::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('registration/register.html.twig', [
                'form' => $form->createView(),
            ]);

        }

        $data = $form->getData();
        $doctorType = $this->doctorTypeRepository->find($data['doctor_type']);

        if (null === $doctorType) {
            throw new NotFoundResourceException($doctorType);
        }

        $user = new User();
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        $user->setUsername($data['username']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['plainPassword']));
        $user->setRoles([
            'ROLE_DOCTOR',
        ]);

        $user   = $this->userRepository->save($user);
        $doctor = new Doctor();

        $doctor->setType($doctorType);
        $doctor->setUser($user);

        $this->doctorRepository->save($doctor);

        return $this->redirectToRoute('home', [
            'success' => 'Registration successful.',
        ]);
    }
}