<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Country;
use App\Form\Type\Country\CountryForm;
use App\Repository\CountryRepository;
use App\Repository\PatientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class CountryController extends AbstractController
{
    private CountryRepository $countryRepository;

    private PatientRepository $patientRepository;

    public function __construct(CountryRepository $countryRepository, PatientRepository $patientRepository)
    {
        $this->countryRepository = $countryRepository;
        $this->patientRepository = $patientRepository;
    }

    public function list(): Response
    {
        $countries = $this->countryRepository->findAll();

        return $this->render('country/list.html.twig', [
            'countries' => $countries,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $country = $this->countryRepository->find($id);

        if (null === $country) {
            throw new NotFoundHttpException();
        }

        return $this->render('country/single.html.twig', [
            'country' => $country,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(CountryForm::class, new Country());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('country/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var Country $country */
        $country = $form->getData();
        $country = $this->countryRepository->save($country);

        return $this->redirectToRoute('country_get_single', [
            'id' => $country->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $country = $this->countryRepository->find($id);

        if (null === $country) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(CountryForm::class, $country);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('country/edit.html.twig', [
                'form' => $form->createView(),
                'country' => $country,
            ]);
        }

        /** @var Country $country */
        $country = $form->getData();
        $country = $this->countryRepository->save($country);

        return $this->redirectToRoute('country_get_single', [
            'id' => $country->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $country = $this->countryRepository->find($id);

        if (null === $country) {
            throw new NotFoundHttpException();
        }

        $patientsCount = $this->patientRepository->count([
            'location' => $country,
        ]);

        if ($patientsCount > 0) {
            return $this->redirectToRoute('country_get_single', [
                'id' => $country->getId(),
                'error' => 'You can\'t delete a country related to at least one patient.',
            ]);
        }

        $this->countryRepository->delete($country);

        return $this->redirectToRoute('country_list');
    }
}