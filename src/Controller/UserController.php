<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\User\UserCreateForm;
use App\Form\Type\User\UserUpdateForm;
use App\Repository\DoctorRepository;
use App\Repository\ExaminationRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserController extends AbstractController
{
    private UserRepository $userRepository;

    private UserPasswordEncoderInterface $passwordEncoder;

    private DoctorRepository $doctorRepository;

    private ExaminationRepository $examinationRepository;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        DoctorRepository $doctorRepository,
        ExaminationRepository $examinationRepository
    ) {
        $this->userRepository   = $userRepository;
        $this->passwordEncoder  = $passwordEncoder;
        $this->doctorRepository = $doctorRepository;
        $this->examinationRepository = $examinationRepository;
    }

    public function list(): Response
    {
        $users = $this->userRepository->findAll();

        return $this->render('user/list.html.twig', [
            'users' => $users,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        return $this->render('user/single.html.twig', [
            'user' => $user,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(UserCreateForm::class, new User());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('user/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var User $user */
        $user = $form->getData();

        if ('' !== $user->getPlainPassword()) {
            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        }

        $user = $this->userRepository->save($user);

        return $this->redirectToRoute('user_get_single', [
            'id' => $user->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(UserUpdateForm::class, $user);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('user/edit.html.twig', [
                'form' => $form->createView(),
                'user' => $user,
            ]);
        }

        /** @var User $user */
        $user = $form->getData();

        if ('' !== $user->getPlainPassword()) {
            $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        }

        $user = $this->userRepository->save($user);

        return $this->redirectToRoute('user_get_single', [
            'id' => $user->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $user = $this->userRepository->find($id);

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        $doctor = $this->doctorRepository->findOneBy([
            'user' => $user,
        ]);

        if (null !== $doctor) {
            $relatedExaminationCount = $this->examinationRepository->count([
                'doctor' => $doctor,
            ]);

            if ($relatedExaminationCount > 0) {
                return $this->redirectToRoute('user_get_single', [
                    'id' => $user->getId(),
                    'error' => 'You can\'t delete a doctor related to at least one examination.',
                ]);
            }

            $this->doctorRepository->delete($doctor);
        }

        $this->userRepository->delete($user);

        return $this->redirectToRoute('user_list');
    }
}