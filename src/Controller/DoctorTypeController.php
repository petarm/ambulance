<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\DoctorType;
use App\Form\Type\DoctorType\DoctorTypeForm;
use App\Repository\DoctorRepository;
use App\Repository\DoctorTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class DoctorTypeController extends AbstractController
{
    private DoctorTypeRepository $doctorTypeRepository;

    private DoctorRepository $doctorRepository;

    public function __construct(DoctorTypeRepository $doctorTypeRepository, DoctorRepository $doctorRepository)
    {
        $this->doctorTypeRepository = $doctorTypeRepository;
        $this->doctorRepository     = $doctorRepository;
    }

    public function list(): Response
    {
        $doctorTypes = $this->doctorTypeRepository->findAll();

        return $this->render('doctorType/list.html.twig', [
            'doctorTypes' => $doctorTypes,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $doctorType = $this->doctorTypeRepository->find($id);

        if (null === $doctorType) {
            throw new NotFoundHttpException();
        }

        return $this->render('doctorType/single.html.twig', [
            'doctorType' => $doctorType,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(DoctorTypeForm::class, new DoctorType());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('doctorType/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var DoctorType $doctorType */
        $doctorType = $form->getData();
        $doctorType = $this->doctorTypeRepository->save($doctorType);

        return $this->redirectToRoute('doctor_type_get_single', [
            'id' => $doctorType->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $doctorType = $this->doctorTypeRepository->find($id);

        if (null === $doctorType) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(DoctorTypeForm::class, $doctorType);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('doctorType/edit.html.twig', [
                'form' => $form->createView(),
                'doctorType' => $doctorType,
            ]);
        }

        /** @var DoctorType $doctorType */
        $doctorType = $form->getData();
        $doctorType = $this->doctorTypeRepository->save($doctorType);

        return $this->redirectToRoute('doctor_type_get_single', [
            'id' => $doctorType->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $doctorType = $this->doctorTypeRepository->find($id);

        if (null === $doctorType) {
            throw new NotFoundHttpException();
        }

        $doctorsCount = $this->doctorRepository->count([
            'type' => $doctorType,
        ]);

        if ($doctorsCount > 0) {
            return $this->redirectToRoute('doctor_type_get_single', [
                'id' => $doctorType->getId(),
                'error' => 'You can\'t delete a doctor type related to at least one doctor.',
            ]);
        }

        $this->doctorTypeRepository->delete($doctorType);

        return $this->redirectToRoute('doctor_type_list');
    }
}