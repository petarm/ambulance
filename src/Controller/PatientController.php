<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Patient;
use App\Form\Type\Patient\PatientForm;
use App\Repository\PatientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class PatientController extends AbstractController
{
    private PatientRepository $patientRepository;

    public function __construct(PatientRepository $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }

    public function list(): Response
    {
        $patients = $this->patientRepository->findAll();

        return $this->render('patient/list.html.twig', [
            'patients' => $patients,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $patient = $this->patientRepository->find($id);

        if (null === $patient) {
            throw new NotFoundHttpException();
        }

        return $this->render('patient/single.html.twig', [
            'patient' => $patient,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(PatientForm::class, new Patient());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('patient/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var Patient $patient */
        $patient = $form->getData();
        $patient = $this->patientRepository->save($patient);

        return $this->redirectToRoute('patient_get_single', [
            'id' => $patient->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $patient = $this->patientRepository->find($id);

        if (null === $patient) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PatientForm::class, $patient);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('patient/edit.html.twig', [
                'form' => $form->createView(),
                'patient' => $patient,
            ]);
        }

        /** @var Patient $patient */
        $patient = $form->getData();
        $patient = $this->patientRepository->save($patient);

        return $this->redirectToRoute('patient_get_single', [
            'id' => $patient->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $patient = $this->patientRepository->find($id);

        if (null === $patient) {
            throw new NotFoundHttpException();
        }

        $this->patientRepository->delete($patient);

        return $this->redirectToRoute('patient_list');
    }
}