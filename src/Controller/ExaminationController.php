<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Examination;
use App\Entity\User;
use App\Form\Type\Examination\DoctorExaminationForm;
use App\Form\Type\Examination\ExaminationForm;
use App\Repository\ExaminationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

final class ExaminationController extends AbstractController
{
    private ExaminationRepository $examinationRepository;

    private Security $security;

    public function __construct(ExaminationRepository $examinationRepository)
    {
        $this->examinationRepository = $examinationRepository;
    }

    public function list(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $isDoctor = in_array('ROLE_DOCTOR', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles());

        $examinations = $isDoctor
            ? $this->examinationRepository->findBy([
                'doctor' => $user->getDoctor(),
            ])
            : $this->examinationRepository->findAll();

        return $this->render('examination/list.html.twig', [
            'examinations' => $examinations,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $examination = $this->examinationRepository->find($id);

        if (null === $examination) {
            throw new NotFoundHttpException();
        }

        return $this->render('examination/single.html.twig', [
            'examination' => $examination,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(ExaminationForm::class, new Examination());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('examination/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var Examination $examination */
        $examination = $form->getData();
        $examination = $this->examinationRepository->save($examination);

        return $this->redirectToRoute('examination_get_single', [
            'id' => $examination->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $examination = $this->examinationRepository->find($id);

        if (null === $examination) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();
        $isDoctor = in_array('ROLE_DOCTOR', $user->getRoles()) && !in_array('ROLE_ADMIN', $user->getRoles());

        if ($isDoctor && $user->getDoctor() !== $examination->getDoctor()) {
            throw new AccessDeniedException();
        }

        $formName = $isDoctor ? DoctorExaminationForm::class : ExaminationForm::class;

        $form = $this->createForm($formName, $examination);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('examination/edit.html.twig', [
                'form' => $form->createView(),
                'examination' => $examination,
            ]);
        }

        /** @var Examination $examination */
        $examination = $form->getData();
        $examination = $this->examinationRepository->save($examination);

        return $this->redirectToRoute('examination_get_single', [
            'id' => $examination->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $examination = $this->examinationRepository->find($id);

        if (null === $examination) {
            throw new NotFoundHttpException();
        }

        $this->examinationRepository->delete($examination);

        return $this->redirectToRoute('examination_list');
    }
}