<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Doctor;
use App\Form\Type\Doctor\DoctorForm;
use App\Repository\DoctorRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class DoctorController extends AbstractController
{
    private DoctorRepository $doctorRepository;

    private UserRepository $userRepository;

    public function __construct(DoctorRepository $doctorRepository, UserRepository $userRepository)
    {
        $this->doctorRepository = $doctorRepository;
        $this->userRepository = $userRepository;
    }

    public function list(): Response
    {
        $doctors = $this->doctorRepository->findAll();

        return $this->render('doctor/list.html.twig', [
            'doctors' => $doctors,
        ]);
    }

    public function getSingle(int $id): Response
    {
        $doctor = $this->doctorRepository->find($id);

        if (null === $doctor) {
            throw new NotFoundHttpException();
        }

        return $this->render('doctor/single.html.twig', [
            'doctor' => $doctor,
        ]);
    }

    public function create(Request $request): Response
    {
        $form = $this->createForm(DoctorForm::class, new Doctor());
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('doctor/create.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        /** @var Doctor $doctor */
        $doctor = $form->getData();
        $doctor = $this->doctorRepository->save($doctor);

        $user = $doctor->getUser();
        $userRoles = $user->getRoles();
        $userRoles[] = 'ROLE_DOCTOR';
        $user->setRoles($userRoles);
        $this->userRepository->save($user);

        return $this->redirectToRoute('doctor_get_single', [
            'id' => $doctor->getId(),
        ]);
    }

    public function update(int $id, Request $request): Response
    {
        $doctor = $this->doctorRepository->find($id);

        if (null === $doctor) {
            throw new NotFoundHttpException();
        }

        $oldUser = $doctor->getUser();

        $form = $this->createForm(DoctorForm::class, $doctor);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('doctor/edit.html.twig', [
                'form' => $form->createView(),
                'doctor' => $doctor,
            ]);
        }

        /** @var Doctor $doctor */
        $doctor = $form->getData();
        $doctor = $this->doctorRepository->save($doctor);

        $newUser = $doctor->getUser();

        if ($oldUser !== $newUser) {
            $oldUser->setRoles(array_filter($oldUser->getRoles(), function (string $role): bool {
                return $role !== 'ROLE_DOCTOR';
            }));
            $newUserRoles = $newUser->getRoles();
            $newUserRoles[] = 'ROLE_DOCTOR';
            $newUser->setRoles($newUserRoles);

            $this->userRepository->save($oldUser);
            $this->userRepository->save($newUser);
        }

        return $this->redirectToRoute('doctor_get_single', [
            'id' => $doctor->getId(),
        ]);
    }

    public function delete(int $id): Response
    {
        $doctor = $this->doctorRepository->find($id);

        if (null === $doctor) {
            throw new NotFoundHttpException();
        }

        $this->doctorRepository->delete($doctor);

        return $this->redirectToRoute('doctor_list');
    }
}