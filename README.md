# Ambulance

```bash
# Database Migration
bin/console doctrine:schema:update --force

# Database Fixtures
bin/console doctrine:fixtures:load

# Server Start
symfony server:start --no-tls
```
